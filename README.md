# Soul Vessel Engine v0.0.0_alpha

[![pipeline status](https://gitlab.com/Chargebrains/soulvesselengine/badges/main/pipeline.svg)](https://gitlab.com/Chargebrains/soulvesselengine/-/commits/main)

Hi guys. This is my little Soul Vessel game engine.
The engine was created for the purpose of self-learning and is (or will be) based on trace technologies: C++20, STL,
Boost, SDL2, OpenGL ES 3.0, WebAsm, WebGL.
In the future it is planned to support the following platforms: Win, Linux, Android, Web. Someday also Mac, IOS, UWP