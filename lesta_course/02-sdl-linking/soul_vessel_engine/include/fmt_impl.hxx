#pragma once

namespace soul_vessel_engine::fmt
{
template <typename T>
void format_helper(std::ostringstream& oss, std::string_view& str, const T& value)
{
    std::size_t openBracket = str.find('{');
    if (openBracket == std::string::npos)
    {
        return;
    }
    std::size_t closeBracket = str.find('}', openBracket + 1);
    if (closeBracket == std::string::npos)
    {
        return;
    }

    oss << str.substr(0, openBracket) << value;
    str = str.substr(closeBracket + 1);
}

template <typename... Args>
std::string format_string::format(Args&&... args)
{
    std::ostringstream oss;
    (format_helper(oss, str_, std::forward<Args>(args)), ...);
    oss << str_;
    return oss.str();
}
} // namespace soul_vessel_engine::fmt