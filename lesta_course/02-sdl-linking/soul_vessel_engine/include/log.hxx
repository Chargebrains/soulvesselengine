#pragma once

#include <string_view>

namespace soul_vessel_engine::log
{
auto trace(std::string_view log) -> void;

auto debug(std::string_view log) -> void;

auto info(std::string_view log) -> void;

auto warning(std::string_view log) -> void;

auto error(std::string_view log) -> void;

auto fatal(std::string_view log) -> void;
} // namespace soul_vessel_engine::log
