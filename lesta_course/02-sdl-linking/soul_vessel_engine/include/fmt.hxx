#pragma once

#include <cstddef>
#include <iostream>
#include <sstream>

namespace soul_vessel_engine::fmt
{
class format_string
{
public:
    explicit format_string(const char* str, size_t len)
        : str_{ str, len } {};

    template <typename... Args>
    auto format(Args&&... args) -> std::string;

private:
    std::string_view str_;
};
} // namespace soul_vessel_engine::fmt

static auto operator"" _fmt(const char* str, size_t len) -> soul_vessel_engine::fmt::format_string
{
    return soul_vessel_engine::fmt::format_string{ str, len };
}

#include "fmt_impl.hxx" // dirty hack for bad boys