#include "log.hxx"
#include "fmt.hxx"

namespace soul_vessel_engine::log_implementation
{
auto print_log(std::ostream& os, std::string_view log_level, std::string_view log) -> void
{
    os << "{}:{}"_fmt.format(log_level, log) << std::endl;

    if (os.fail())
    {
        throw std::runtime_error{ "Error: output stream failed" };
    }
}
} // namespace soul_vessel_engine::log_implementation

namespace soul_vessel_engine::log
{
auto trace(std::string_view log) -> void
{
    log_implementation::print_log(std::cout, "TRACE", log);
}

auto debug(std::string_view log) -> void
{
    log_implementation::print_log(std::cout, "DEBUG", log);
}

auto info(std::string_view log) -> void
{
    log_implementation::print_log(std::cout, "INFO", log);
}

auto warning(std::string_view log) -> void
{
    log_implementation::print_log(std::cout, "WARNING", log);
}

auto error(std::string_view log) -> void
{
    log_implementation::print_log(std::cerr, "ERROR", log);
}

auto fatal(std::string_view log) -> void
{
    log_implementation::print_log(std::cerr, "FATAL", log);
}

} // namespace soul_vessel_engine::log