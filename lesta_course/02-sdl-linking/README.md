# 02-SDL-LINKING

Hello. This is Lesta's second [homework](https://github.com/leanid/om/tree/master/01-basic-game-dev#%D0%B4%D0%B7-02)
course.

## Project Description

This is a project example of linking sdl dynamically and statically.
In the project, you can build shared and static version and also build for the web
with [Emscripten](https://emscripten.org/index.html)

## Build  And  Run Example

### Before start examples

* clone and install [SDL3](https://github.com/libsdl-org/SDL)
* install [Emscripten](https://emscripten.org/index.html)

### UNIX

#### Build

``` 
mkdir build
cd build 
cmake -DBUILD_STATIC=ON -DBUILD_SHARED=ON ..
cmake --build .
```

#### Run Shared

```
./02-sdl-shared 
```

#### Run Static

```
./02-sdl-static 
```

#### Check All Dependencies

```
ldd 02-sdl-shared
```

```
ldd 02-sdl-static
```

#### Check Only SDL Dependencies

```
ldd 02-sdl-shared | grep SDL
```

```
ldd 02-sdl-static | grep SDL
```

#### Fun moment
if you're bored, then try this thing [SDL-dynapi](https://github.com/libsdl-org/SDL/blob/main/docs/README-dynapi.md)

### EMSCRIPTEN

#### Build

``` 
mkdir build
cd build 
emcmake cmake -DBUILD_STATIC=ON -DBUILD_SHARED=OFF ..
cmake --build .
```

#### Run

```
node 02-sdl-static.js 
```

### TROUBLESHOOTING

* Don't forget build and install SDL3 for Emscripten. Use Emscripten toolchain for that. See ```emcmake```
  and ```Emscripten.cmake``` 
* Remember that Web build doesn't support shared build. Don't forget ```-DBUILD_SHARED=OFF -DBUILD_SHARED=OFF```
  
* ...OTHER COMMING SOON... SORRY:(