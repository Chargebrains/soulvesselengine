#include <cstdlib>
#include <iostream>

#include <SDL3/SDL_version.h>

#include "soul_vessel_engine/include/fmt.hxx"
#include "soul_vessel_engine/include/log.hxx"

auto print_sdl_lib_info() -> void;

int main(int, char**)
{
    namespace sve = soul_vessel_engine;

    try
    {
        print_sdl_lib_info();
        return EXIT_SUCCESS;
    }
    catch (const std::exception& e)
    {
        sve::log::fatal(e.what());
    }
    catch (...)
    {
        sve::log::fatal("Something really bad. I recommend contacting my author to tell him about it. Sorry:(");
    }

    return EXIT_FAILURE;
}

std::ostream& operator<<(std::ostream& out, const SDL_version& v)
{
    out << static_cast<int>(v.major) << '.';
    out << static_cast<int>(v.minor) << '.';
    out << static_cast<int>(v.patch);
    return out;
}

auto print_sdl_lib_info() -> void
{
    SDL_version compiled = { 0, 0, 0 };
    SDL_version linked   = { 0, 0, 0 };

    SDL_VERSION(&compiled)
    SDL_GetVersion(&linked);

    soul_vessel_engine::log::info("SDL_VERSION COMPILED: {}. SDL_VERSION LINKED: {}"_fmt.format(compiled, linked));
}
