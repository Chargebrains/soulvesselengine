#include <cstdlib>

#include <meow_lib.hpp>

int main(int, char**)
{
    const bool is_good = print_meow();
    return is_good ? EXIT_SUCCESS : EXIT_FAILURE;
}
